/*
 * This file is part of the nxlog log collector tool.
 * See the file LICENSE in the source root for licensing terms.
 * Website: http://nxlog.org
 * Author: Botond Botyanszki <botond.botyanszki@nxlog.org>
 */

#include <apr_lib.h>

#include "../../../common/module.h"
#include "syslog.h"

#define NX_LOGMODULE NX_LOGMODULE_CORE


void nx_expr_func__xm_multiline_process_multiline(nx_expr_eval_ctx_t *eval_ctx UNUSED,
						  nx_module_t *module UNUSED,
						  nx_value_t *retval,
						  int32_t num_arg,
						  nx_value_t *args)
{
    ASSERT(retval != NULL);
    ASSERT((num_arg == 1) || (num_arg == 0));

    if ( num_arg == 1 )
    {
	if ( args[0].type != NX_VALUE_TYPE_STRING )
	{
	    throw_msg("invalid '%s' type argument for function 'process_multiline(string)'",
		      nx_value_type_to_string(args[0].type));
	}
    }

    retval->defined = TRUE;
    retval->type = NX_VALUE_TYPE_BOOLEAN;
    if ( args[0].defined == FALSE )
    {
	retval->boolean = FALSE;
	return;
    }

    //retval->boolean = TRUE;
}



void nx_expr_proc__xm_multiline_process_multiline(nx_expr_eval_ctx_t *eval_ctx,
						  nx_module_t *module UNUSED,
						  nx_expr_list_t *args)
{
    nx_expr_list_elem_t *arg;
    nx_value_t value;

    if ( eval_ctx->logdata == NULL )
    {
	throw_msg("no logdata available for process_multiline(), possibly dropped");
    }

    if ( (args != NULL) && ((arg = NX_DLIST_FIRST(args)) != NULL) )
    {
	ASSERT(arg->expr != NULL);
	nx_expr_evaluate(eval_ctx, &value, arg->expr);

	if ( value.defined != TRUE )
	{
	    throw_msg("source string is undef");
	}
	if ( value.type != NX_VALUE_TYPE_STRING )
	{
	    nx_value_kill(&value);
	    throw_msg("string type required for 'line'");
	}
	//nx_syslog_parse_rfc5424(eval_ctx->logdata, value.string->buf, value.string->len);
	nx_value_kill(&value);
    }
    else
    {
	if ( nx_logdata_get_field_value(eval_ctx->logdata, "raw_event", &value) == FALSE )
	{
	    throw_msg("raw_event field missing");
	}
	if ( value.defined != TRUE )
	{
	    throw_msg("raw_event field is undef");
	}
	if ( value.type != NX_VALUE_TYPE_STRING )
	{
	    throw_msg("string type required for field 'raw_event'");
	}
	//nx_syslog_parse_rfc5424(eval_ctx->logdata, value.string->buf, value.string->len);
    }
}

