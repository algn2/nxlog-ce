// The contents of this file and en/attributes.adoc should roughly correspond
// with the attributes.adoc file in the nxlog/documentation repository.

:source-highlighter: coderay
:toc: left
:toclevels: 2
:sectanchors:
:idprefix:
:idseparator: -
:experimental:
:lang: en-US
:sectnums: true
:sectnumlevels: 5
:attribute-missing: warn
:docinfo1:

:pdf-stylesdir': ../theme
:pdf-fontsdir: ../theme/fonts
:pdf-style: nxlog-pdf

:imagesdir: ../images

// ifeval::["{backend}" == "pdf"]
// :imagesdir: images
// endif::[]

// ifeval::["{backend}" == "html5"]
// :imagesdir: images
// endif::[]
